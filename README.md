# GTK website

A draft for the new GTK website style and content.

The theme is forked from https://wiki.gnome.org/GnomeWeb/Deneb to keep consistency.

## Running the project
Clone the repo
```
git clone https://gitlab.gnome.org/davcri/gtk-web.git
cd gtk-web/src
```
Install NPM dependencies
```
npm install
```
Run the project
```
npm start
```
Visit local webserver http://localhost:8000/ to see or edit the created theme.

## Collaborate 
Take a look at the [issues](https://gitlab.gnome.org/davcri/gtk-web/issues)
because in this early phase we have to take decisions about the techology to
use.
